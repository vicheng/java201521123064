package test13;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class MenuServer {
	// 201521123064
	public static void main(String[] args) throws IOException {
		ServerSocket s = new ServerSocket(8190);
		int i = 0;
		Socket income;
		while (true) {
			income = s.accept();
			System.out.println("第" + i + "个客户端");
			Thread t = new Thread(Runnable(new Threaded(income)));
			t.start();
			i++;
		}
		try {
			InputStream ins = income.getInputStream();
			OutputStream outs = income.getOutputStream();
			Scanner sc = new Scanner(ins);
			PrintWriter pr = new PrintWriter(outs, true);
			pr.println("输入menu显示菜单,输入quit退出程序");
			while (sc.hasNextLine()) {
				String line = sc.nextLine().trim();
				// sc.next();
				System.out.println(line);
				if (line.trim().equals("quit")) {
					pr.println("再见！");
					break;
				} else if (line.trim().equals("menu")) {
					pr.println("1.显示服务器当前时间");
					pr.println("2.您的IP");
					pr.println("3.文件搜索");
					pr.println("4.退出菜单");
				} else if (line.trim().equals("1")) {
					pr.println("你的选择：1");
					SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					pr.println("	" + time.format(new Date()));
				} else if (line.trim().equals("2")) {
					pr.println("你的选择：2");
					pr.println("	" + income.getInetAddress());
				} else if (line.trim().equals("3")) {
					pr.println("你的选择：3");
					pr.println("	" + "尚未完成");
				} else if (line.trim().equals("4")) {
					pr.println("你的选择：4");
					pr.println("	" + "再见！");
					break;
				}
			}
		} finally {
			income.close();
		}
	}

	private static Runnable Runnable(Threaded threaded) {
		// TODO Auto-generated method stub
		return null;
	}
}

public class Threaded implements Runnable {

	public Threaded(Socket income) {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}

}