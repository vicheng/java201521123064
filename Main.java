import java.util.Scanner;

class Repo {
	public String[] Items;

	public Repo(String items) {
		this.Items = items.split(" ");
	}
}

class Worker1 implements Runnable {
	public Worker1(Repo repo) {
		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}
}

class Worker2 implements Runnable {
	public Worker2(Repo repo) {

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}
}

public class Main {
	public static void main(String[] args) throws InterruptedException {
		Scanner sc = new Scanner(System.in);
		Repo repo = new Repo(sc.nextLine());
		Thread t1 = new Thread(new Worker1(repo));
		Thread t2 = new Thread(new Worker2(repo));
		t1.start();
		Thread.yield();
		t2.start();
		sc.close();
	}
}