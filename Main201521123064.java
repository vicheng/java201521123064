package test12;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Main201521123064 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while (sc.hasNextLine()) {
			System.out.println(JudgeNum(sc.nextLine()));
		}
	}

	// 201521123064
	public static boolean JudgeNum(String string) {
		String pattern = "[+-]?[0-9]+";
		return Pattern.matches(pattern, string);
	}
}