package test;

public interface IntegerStack {
	public Integer push(Integer item);//如果item为null，则不入栈直接返回null。如果栈满，抛出`FullStackException`。
	public Integer pop();//出栈。如果栈空，抛出EmptyStackException。否则返回
	public Integer peek();//获得栈顶元素。如果栈空，抛出EmptyStackException。
}
