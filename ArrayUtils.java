package test;

import java.util.Scanner;

public class ArrayUtils {

	public static double findMax(double[] arr, int begin, int end) throws IllegalArgumentException {
		if (begin >= end)
			throw new IllegalArgumentException("begin:" + begin + " >= end:" + end);
		else if (begin < 0)
			throw new IllegalArgumentException("begin:" + begin + " < 0");
		else if (end > arr.length)
			throw new IllegalArgumentException("end:" + end + " > arr.length");
		double max = arr[begin];
		for (int i = begin; i < end; i++) {
			if (max < arr[i])
				max = arr[i];
		}
		return max;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int begin, end;
		double[] arr = new double[n];
		for (int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();
		}
		while (sc.hasNextLine()) {
			try {
				String s1 = sc.next();
				begin = Integer.parseInt(s1);
				String s2 = sc.next();
				end = Integer.parseInt(s2);
				System.out.println(findMax(arr, begin, end));
			} catch(NumberFormatException e){
				break;
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		try {
			System.out.println(ArrayUtils.class.getDeclaredMethod("findMax", double[].class, int.class, int.class));
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}