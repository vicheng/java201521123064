package test;

class ArrayIntegerStack implements IntegerStack {
	private int capacity;// 代表内部数组的大小
	private int top = 0;// 代表栈顶指针。栈空时，初始值为0。
	private Integer[] arrStack;// 用于存放元素的数组

	/* 其他代码 */
	/* 你的答案，即3个方法的代码 */
	@Override
	public Integer push(Integer item) {
		// 如果item为null，则不入栈直接返回null。如果栈满，抛出`FullStackException`。
		if (item == null)
			return null;
		if (top == arrStack.length) {
			throw new FullStackException();
		}
		arrStack[top++] = item;
		return item;
	}

	@Override
	public Integer pop() {
		// 出栈。如果栈空，抛出EmptyStackException。否则返回
		if (top == 0) {
			throw new EmptyStackException();
		}
		return arrStack[--top];
	}

	@Override
	public Integer peek() {
		// 获得栈顶元素。如果栈空，抛出EmptyStackException。
		if (top == 0) {
			throw new EmptyStackException();
		}
		return arrStack[top - 1];
	}

}