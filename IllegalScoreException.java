package test;

public class IllegalScoreException extends Exception {

	public IllegalScoreException(String message) {
		super(message);
	}
	
}
