﻿package dao;

import java.util.List;
import entity.Student;

public class TestDaoMain {

	public static void main(String[] args) {
		Student[] students = new Student[3];
		students[0] = new Student(1, "2015201", "Tom", 19, "1998-01-15");
		students[1]= new Student(2, "2015202", "Jerry", 20, "1997-03-05");
		students[2] = new Student(3, "2015203", "Sophia", 22, "1996-10-25");
		
//		StudentDao sdm = new StudentDaoArrayImpl(50);//使用数组实现
//		StudentDao sdm = new StudenDaoListImpl();//使用列表实现
		StudentDao sdm = new StudentDaoJDBCImpl();
		
		System.out.println("===========写入学生========");
		for(entity.Student e:students){
			sdm.add(e);
		}
		System.out.println("===========显示所有学生========");
		sdm.findAll();
		System.out.println("===========查询学生========");
		List<Student> temp = sdm.findByName("Tom") ;
		if(temp == null){
			System.out.println("查无此人");
		}else{
			System.out.println(temp);
		}
	}

}
