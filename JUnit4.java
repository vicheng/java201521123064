package test12;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import org.junit.Test;

import jdk.nashorn.internal.ir.annotations.Ignore;

public class JUnit4 {

	@Test
	public void PrintWrite() {
		int count = 1000 * 10000;
		PrintWriter out = null;
		try {
			out = new PrintWriter("d:/Lines.txt");
			for (int i = 0; i < count; i++) {
				out.println("1");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void BufferedWrite() {
		int count = 1000 * 10000;
		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new FileWriter(new File("d:/Lines1.txt")));
			for (int i = 0; i < count; i++) {
				out.write("1\n");;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Ignore
	public void BufferedRead() {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(new File("d:/Lines.txt")));
			while (br.readLine() != null) {
			} // 只是读出，不进行任何处理
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Ignore
	public void ScannerRead() {
		Scanner scanner=null;
		try {
			scanner = new Scanner(new File("d:/Lines.txt"));
			while(scanner.hasNextLine()){//只是读出每一行，不做任何处理
				scanner.nextLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}finally{
			scanner.close();
		}
	}
}
