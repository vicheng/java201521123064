package test;

public class IllegalNameException extends RuntimeException {

	public IllegalNameException(String message) {
		super(message);
	}

}
