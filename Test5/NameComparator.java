package Test5;

import java.util.Comparator;

public class NameComparator implements Comparator<PersonSortable>{

	public int compare(PersonSortable o1, PersonSortable o2) {
		int x = o1.getName().compareTo(o2.getName());
		return x;
	}
	
}
