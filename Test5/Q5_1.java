package Test5;

import java.util.Arrays;
import java.util.Scanner;

public class Q5_1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		PersonSortable[] persons = new PersonSortable[n];
		for(int i = 0; i < n; i++){
			persons[i] = new PersonSortable(sc.next(), sc.nextInt());
		}
		
		System.out.println("NameComparator:sort");
		Arrays.sort(persons,new NameComparator());
		for(int i = 0; i < n; i++){
			System.out.println(persons[i]);
		}
		
		System.out.println("AgeComparator:sort");
		Arrays.sort(persons,new AgeComparator());
		for(int i = 0; i < n; i++){
			System.out.println(persons[i]);
		}
		
		System.out.println(Arrays.toString(NameComparator.class.getInterfaces()));
		System.out.println(Arrays.toString(AgeComparator.class.getInterfaces()));
	}

}
