package Test5;

public class PersonSortable implements Comparable<PersonSortable>{
	private String name;
	private int age;
	
	public PersonSortable(String name, int age){
		this.name = name;
		this.age = age;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return name + "-" + age;
	}

	@Override
	public int compareTo(PersonSortable o) {
		int x = this.getName().compareTo(o.getName());
		if(x != 0) return x;
		return 0;
	}
	
	
	
}
