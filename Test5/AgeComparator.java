package Test5;

import java.util.Comparator;

public class AgeComparator implements Comparator<PersonSortable>{

	public int compare(PersonSortable o1, PersonSortable o2) {
		return o1.getAge() - o2.getAge();
	}
	
}
