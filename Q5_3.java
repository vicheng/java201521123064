package java3;

import java.util.Arrays;
import java.util.Scanner;

public class Q5_3 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Rectangle[] r = new Rectangle[2];
		Circle[] c = new Circle[2];
		int sumPerimeter=0;
		int sumArea=0;
		for (int i = 0; i < 2; i++) {
			int a = sc.nextInt();
			int b = sc.nextInt();
			r[i] = new Rectangle(b,a);
			sumPerimeter = sumPerimeter + r[i].getPerimeter(b, a);
			sumArea = sumArea + r[i].getArea(b, a);
		}
		for (int i = 0; i <2; i++) {
			int d = sc.nextInt();
			c[i] = new Circle(d);
			sumPerimeter = sumPerimeter + c[i].getPerimeter(d);
			sumArea = sumArea + c[i].getArea(d);
		}
		System.out.println(sumPerimeter);
		System.out.println(sumArea);
		System.out.println(Arrays.toString(r));
		System.out.println(Arrays.toString(c));
	}

}
class Circle {

	private int radius;
	public Circle(int radius){
		this.radius = radius;
	}
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}
	public int getPerimeter(int radius){
		return (int)(Math.PI*radius*2);
	}
	public int getArea(int radius){
		return (int)(Math.PI*radius*radius);
	}
	public int getRadius() {
		return radius;
	}
	public void setRadius(int radius) {
		this.radius = radius;
	}
	
}
class Rectangle {
	private int width;
	private int length;
	public Rectangle(int length, int width){
			this.length = length;
			this.width = width;
	}
	public String toString() {
		return "Rectangle [width=" + width + ", length=" + length + "]";
	}
	public int getPerimeter(int length,int width){
		return (length * 2 + width * 2);
	}
	public int getArea(int length,int width){
		return (int)(length * width);
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
}