package test13;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientListener implements ActionListener {
ClientChart clientChart;
ObjectOutputStream objectout;
ObjectInputStream objectin;
Socket socket;
public ClientListener(ClientChart clientChart){
	this.clientChart = clientChart;
}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==clientChart.getButStart()){
			
			int port = Integer.parseInt(clientChart.getTextPort().getText());
			
			//建立连接
			try {
				socket= new Socket("127.0.0.1",port);
				
				objectin = new ObjectInputStream(socket.getInputStream());
				
				objectout = new ObjectOutputStream(socket.getOutputStream());
				
				//一直读取服务端的消息
				new ClientReadThread(objectin, clientChart).start();
				
				clientChart.getButStart().setEnabled(false);
				
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			
		}
		if(e.getSource()==clientChart.getButSend()){
			
			String mgs = clientChart.getTextInfo().getText();
			
			try {
				objectout.writeObject(mgs);
				
				objectout.flush();
				clientChart.getTextInfo().setText("");
				
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}			
		}
	}

}
