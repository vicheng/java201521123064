﻿package dao;

import java.util.List;
import entity.Student;

public interface StudentDao {
	public int add(Student stu); // 添加成功返回1，否则返回-1

	public int delete(int sid); // 删除成功返回1，否则返回-1

	public int update(Student stu); // 更新成功返回1，否则返回-1

	public List<entity.Student> findAll(); // 查找所有学生

	public entity.Student findById(String sid);// 根据sid查找学生

	public List<entity.Student> findByName(String name);// 根据姓名查找所有学生，使用%进行模糊匹配
}
