package test13;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * This program makes a socket connection to the atomic clock in Boulder,
 * Colorado, and prints the time that the server sends.
 * 
 * @version 1.20 2004-08-03
 * @author Cay Horstmann
 */

public class Socket2015064 {              // 类名出现学号
	public static void main(String[] args) throws IOException {
		Socket socket = new Socket("127.0.0.1", 10000);
		try {
			InputStream inStream = socket.getInputStream();
			Scanner sc = new Scanner(inStream);
			Scanner scan = new Scanner(System.in);
			String line1 = scan.nextLine();
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			pw.println(line1);
			pw.flush();
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				System.out.println(line);
			}
			sc.close();
			scan.close();
		} finally {
			socket.close();
		}
	}
}
