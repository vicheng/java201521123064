package Q5_1;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		Person[] persons = new Person [n];
		for(int i = 0; i < persons.length; i++){
			Person person = new Person(sc.next(), sc.nextInt(), sc.nextBoolean());
			persons[i] = person;
		}
		for(int i = persons.length - 1; i >= 0; i--){
			System.out.println(persons[i]);
		}
		System.out.println(new Person());
	}
}
