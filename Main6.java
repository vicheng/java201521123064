package test11;

import java.util.Scanner;

class Repo {
	private String[] Items;
	public int Size;
	static int i = 0;
	private boolean flag = false;

	public Repo(String items) {
		this.Items = items.split(" ");
		this.Size = Items.length;
	}

	public synchronized void Finish1() {
		while (flag) {
			try {
				wait();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (this.i != this.Size) {
			flag = true;
			System.out.println(Thread.currentThread().getName() + " finish" + " " + Items[i++]);
			notify();
		}
	}

	public synchronized void Finish2() {
		while (!flag) {
			try {
				wait();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (i != this.Size) {
			flag = false;
			System.out.println(Thread.currentThread().getName() + " finish" + " " + Items[i++]);
			notify();
		}
	}
}

class Worker1 implements Runnable {
	public Repo repo;

	public Worker1(Repo repo) {
		this.repo = repo;
	}

	@Override
	public void run() {
		while (repo.i != repo.Size) {
			repo.Finish1();
		}
	}
}

class Worker2 implements Runnable {
	public Repo repo;

	public Worker2(Repo repo) {
		this.repo = repo;
	}

	@Override
	public void run() {
		while (repo.i != repo.Size) {
			repo.Finish2();
		}
	}
}

public class Main6 {
	public static void main(String[] args) throws InterruptedException {
		Scanner sc = new Scanner(System.in);
		Repo repo = new Repo(sc.nextLine());
		Thread t1 = new Thread(new Worker1(repo));
		Thread t2 = new Thread(new Worker2(repo));
		t1.start();
		Thread.yield();
		t2.start();
		sc.close();
	}
}

/*
import java.util.Scanner;
class Repo {
	private int size;
	public int locat = 0;
	public String[] str;
	public boolean cur = false;

	public synchronized int getSize() {
		return this.size;
	}

	public Repo(String items) {
		this.str = items.split(" ");
		this.size = str.length;
	}

	public synchronized void f1() {
		while (cur) {
			try {
				wait();
			} catch (Exception e) {

			}
		}
		if (this.locat != this.getSize()) {
			cur = true;
			System.out.println(Thread.currentThread().getName() + " finish" + " " + str[locat++]);
			notify();
		}
	}

	public synchronized void f2() {
		while (!cur) {
			try {
				wait();

			} catch (Exception e) {

			}
		}
		if (locat != this.getSize()) {
			cur = false;
			System.out.println(Thread.currentThread().getName() + " finish" + " " + str[locat++]);
			notify();
		}
	}
}

class Worker1 implements Runnable {
	public int size;
	Repo repo;

	public Worker1(Repo repo) {
		this.repo = repo;
		this.size = repo.getSize();
	}

	@Override
	public void run() {
		while (repo.locat!=repo.getSize()) {
			repo.f1();

		}
	}
}

class Worker2 implements Runnable {
	public int size;
	Repo repo;

	public Worker2(Repo repo) {
		this.repo = repo;
		this.size = repo.getSize();
	}

	@Override
	public void run() {
		while (repo.locat!=repo.getSize()) {
			repo.f2();
		}
	}
}

 */