package Test3;

import java.util.Scanner;

public class Q5_1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		Person[] persons = new Person [n];
		for(int i = 0; i < persons.length; i++){
			persons[i] = new Person(sc.next(), sc.nextInt(), sc.nextBoolean());
			//persons[i] = person;
		}
		for(int i = persons.length - 1; i >= 0; i--){
			System.out.println(persons[i]);
		}
		System.out.println(new Person());
	}
}
class Person {
	private String name;
	private boolean gender;
	private int age;
	private static int num;
	private int id;
	
	static{
		System.out.println("This is static initialization block");
	}
	{
		id = num++;
		System.out.println("This is initialization block, id is " + id);
	}
	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", gender=" + gender + ", id=" + id + "]";
	}
	public Person(String name, int age, boolean gender){
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
	public Person(){
		System.out.println("This is constructor");
		System.out.println(name + "," + age + "," + gender + "," + id);
		//System.out.println("null,0,false,0");
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isGender() {
		return gender;
	}
	public void setGender(boolean gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}