package test11;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account {
	private int balance;
	private Lock lock = new ReentrantLock();
	private Condition condition = lock.newCondition();

	public Account(int balance) {
		super();
		this.balance = balance;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public void deposit(int money) {
		lock.lock();
		try {
			this.balance += money;
			condition.signal();
		} finally {
			lock.unlock();
		}
	}

	public void withdraw(int money) {
		lock.lock();
		try {
			while (this.balance - money < 0) {
				try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			this.balance -= money;
			condition.signal();
		} finally {
			lock.unlock();
		}
	}
}