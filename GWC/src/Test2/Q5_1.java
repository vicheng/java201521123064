package Test2;

import java.util.Arrays;
import java.util.Scanner;

public class Q5_1 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String[] arr = {"fib", "sort", "search", "getBirthDate"};
		int Fibonacci[] = new int [100];
		Fibonacci[1] = 1;
		Fibonacci[2] = 1;
		for(int i = 3; i < Fibonacci.length; i++)
			Fibonacci[i] = Fibonacci[i - 1] + Fibonacci[i - 2];
		int[] num = null;
		String eat;
		
		while(in.hasNextLine()){
			String str = in.nextLine();
			
			if(str.equals(arr[0])){
				int n = in.nextInt();
				for(int i = 1; i <= n; i++)
					System.out.print(Fibonacci[i] + " ");
				System.out.println();
				eat = in.nextLine();
			}
			
			else if(str.equals(arr[1])){
				String[] string = in.nextLine().split(" ");   //以“ ”为条件进行split
				num = new int [string.length];
				for(int i = 0; i < num.length; i++){
					num[i] = Integer.parseInt(string[i]);
				}
				Arrays.sort(num);
				System.out.println(Arrays.toString(num));
			}
			
			else if(str.equals(arr[2])){
				int string = in.nextInt();
				if(Arrays.binarySearch(num, string) >= 0)
					System.out.println(Arrays.binarySearch(num, string));
				else
					System.out.println("-1");
				eat = in.nextLine();
			}
			
			else if(str.equals(arr[3])){
				int n = in.nextInt();
				String id, year, month, day;
				for(int i = 0; i < n; i++){
					id = in.next();
					year = id.substring(6, 10);
					month = id.substring(10, 12);
					day = id.substring(12, 14);
					System.out.println(year + "-" + month + "-" + day);
				}
				eat = in.next();
			}
			
			else
				System.out.println("exit");
		}
	}

}
