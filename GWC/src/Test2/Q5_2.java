package Test2;

import java.util.Scanner;

public class Q5_2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		StringBuilder str = new StringBuilder();
		while(sc.hasNext()){
			int n = sc.nextInt(), x = sc.nextInt(), y = sc.nextInt();
			for(int i = 0; i < n; i++)
				str.append(i);
			System.out.println(str.substring(x, y));
		}
	}
}
