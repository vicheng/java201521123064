package Test2;

import java.util.Arrays;
import java.util.Scanner;

public class Q5_4 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		while(sc.hasNextInt()){
		int n = sc.nextInt();
		String[][] arr = new String[n][];
		for (int x = 1; x <= n; x++) {
			arr[x-1]=new String[x];
			for(int y = 1; y <= x; y++){
				arr[x-1][y-1]=x+"*"+y+"="+x*y;//xiabiao
				if(y==x) System.out.print(String.format("%s", arr[x-1][y-1]));
				else System.out.print(String.format("%-7s", arr[x-1][y-1]));
			}
			System.out.println("");
		}
		System.out.println(Arrays.deepToString(arr));
		}
		}
		
}