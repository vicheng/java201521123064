package Test2;

import java.math.BigDecimal;
import java.util.Scanner;

public class Q5_5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		while(sc.hasNextBigDecimal()){
			BigDecimal a=sc.nextBigDecimal();
			BigDecimal b=sc.nextBigDecimal();
			BigDecimal res1=new BigDecimal(0);
			BigDecimal res2=new BigDecimal(1);
			res1=res1.add(a);
			res1=res1.add(b);
			res2=res2.multiply(a);
			res2=res2.multiply(b);
			System.out.println(res1);
			System.out.println(res2);
		}
	}

}