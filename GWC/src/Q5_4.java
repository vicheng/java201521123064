import java.util.Scanner;

public class Q5_4 {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		while(in.hasNextLine()){
			double epsilon = 0.0001, x = in.nextDouble(), y = 0;
			if (x < 0)
				System.out.println(Double.NaN);
			else{
				while(x - Math.pow(y, 2) >= epsilon && x >= Math.pow(y, 2)){
					y += 0.0001;
				}
				System.out.printf("%.6f\n", y);
			}
		}
	}
}
