import java.util.Scanner;

public class Q5_3 {
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		while(in.hasNextLine()){
			String s1,s2;
			int sum = 0;
			s1 = in.nextLine();
			s1 = s1.replace("-", "");
			s1 = s1.replace(".", "");
			for(int i = 0; i < s1.length(); i++){
				s2 = s1.substring(i, i+1);
				sum += Integer.parseInt(s2);
			}
			System.out.println(sum);
			}
		}
	}
