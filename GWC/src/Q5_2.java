import java.util.Scanner;

public class Q5_2 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while(in.hasNextInt()){
			int num = in.nextInt();
			if (num >= 10000 && num <= 20000){
				System.out.println(Integer.toBinaryString(num) + ',' + Integer.toOctalString(num) + ',' + Integer.toHexString(num));
			}
			else{
				if (num < 0) num = -num;
				String str1 = Integer.toString(num), str2;
				int sum = 0;
				for (int i = 0; i < str1.length(); i++){
					str2 = str1.substring(i, i+1);
					System.out.print(str2 + " ");
					sum += Integer.parseInt(str2);
				}
				System.out.println(sum);
			}
		}
	}
}
