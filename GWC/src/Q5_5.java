import java.util.Scanner;

public class Q5_5 {
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		while(in.hasNextLine()){
			String s;
			int a = 0, n = 0;
			a = in.nextInt();
			if (a == 0)
				System.out.println(32);
			else{
				s = Integer.toBinaryString(a);
				n = 32 - s.length();
				System.out.println(n);
			}
		}
	}
}
