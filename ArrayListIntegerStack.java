package test;

import java.util.ArrayList;
import java.util.List;

public class ArrayListIntegerStack implements IntegerStack {
	private List<Integer> list;

	public ArrayListIntegerStack() {
		list = new ArrayList<Integer>();
	}

	@Override
	public Integer push(Integer item) {
		// 如item为null，则不入栈直接返回null。如栈满，也返回null.
		if (item == null)
			return null;
		list.add(item);
		return item;
	}

	@Override
	public Integer pop() {
		// 出栈，如为空，则返回null.
		if (list.isEmpty())
			return null;
		return list.remove(list.size() - 1);
	}

	@Override
	public Integer peek() {
		// 获得栈顶元素，如为空，则返回null.
		if (list.isEmpty())
			return null;
		return list.get(list.size() - 1);
	}

	@Override
	public boolean empty() {
		// 如为空返回true
		if (list.isEmpty())
			return true;
		return false;
	}

	@Override
	public int size() {
		// 返回栈中元素数量
		return list.size();
	}

	@Override
	public String toString() {
		return "" + list;
	}

}
